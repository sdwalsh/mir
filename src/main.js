(function() {
  const commitHash = document.getElementById('commit-hash');
  const commitDate = document.getElementById('commit-date');

  async function getCommitHash(req) {
    return fetch(req)
    .then((res) => {
      if(res.ok) {
        return res.json();
      } else {
        return Promise.reject('Reponse not 200 - 299');
      }
    });
  }

  const req = new Request(
    'https://bitbucket.org/api/2.0/repositories/sdwalsh/mirango/commits?size=1&limit=1', 
    { method: 'GET'});
  getCommitHash(req)
  .then((res) => {
    commitHash.innerHTML = '@' + res.values[0].hash.substring(0, 7);
    commitDate.innerHTML = res.values[0].date.substring(0,10);
  })
  .catch((e) => {
    console.log(e);
  })
})();